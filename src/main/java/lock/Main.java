package lock;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj kombinację do zamknięcia zamka w formacie X-X-X");
        Scanner scanner = new Scanner(System.in);
        String lockCombination = scanner.nextLine();
        String[] lockSplitted = lockCombination.split("-");
        System.out.println(lockSplitted[0]);
        System.out.println(lockSplitted[1]);
        System.out.println(lockSplitted[2]);

        Lock lock = new Lock(Integer.parseInt(lockSplitted[0]), Integer.parseInt(lockSplitted[1]), Integer.parseInt(lockSplitted[2]));

        System.out.println("Tworze zamek lock --> " + lock);

        lock.shuffle();
        lock.switchA();
        lock.switchC();
        System.out.println(lock);

        if (lock.isOpen()) {
            System.out.println("Zamek jest otwarty");
        } else {
            System.out.println("Zamek jest zamknięty");
        }

    }
}
