package aoc2019day1;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {

    public static final Path PATH = Path.of("src/main/resources/adventofcode");

    public static void main(String[] args) throws IOException {
        //masa /3 ->zaokrąglić w dół - 2 ---> paliwo
        //dla każdego wpisu z pliku liczymy paliwo następnie sumujemy wszystko

        var lines = Files.lines(PATH);
        Stream<String> inputValues =
                lines;
        List<String> listOfModuleMass = inputValues.collect(Collectors.toList());

        int sum = 0;
        for (String mass: listOfModuleMass){
            int requiredFuel = calculateFuel(Integer.parseInt(mass));  //robi inta ze stringa
            sum += requiredFuel;
            System.out.println("Fuels needed for mass " + mass + " ->> " + requiredFuel);
        }

        System.out.println("Sum of required fuel: " + sum);
    }

    public static int calculateFuel(int mass) {
        int fuel = (mass / 3) - 2;
        if (fuel <= 0){
            return 0;
        } else {
            return fuel + calculateFuel(fuel);
        }
    }
}
