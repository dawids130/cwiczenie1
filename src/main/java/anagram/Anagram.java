package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwa słowa, sprawdzę czy są anagramami :)");
        System.out.println("Podaj pierwsze słowo");
        String firstword = scanner.nextLine();
        System.out.println("Podaj drugie słowo:");
        String secondword = scanner.nextLine();

        char[] firswordcharacters = firstword.toCharArray();
        char[] secondwordcharacters  = secondword.toCharArray();
        Arrays.sort(firswordcharacters);
        Arrays.sort(secondwordcharacters);

        boolean areEqual = Arrays.equals(firswordcharacters, secondwordcharacters);

        if (areEqual) {
            System.out.println("Podane słowa sa anagramami");
        } else {
            System.out.println("Podane słowa nie sią anagramami");
        }
    }
}
