1. Stwórz typ Lock, reprezentujący zamek na kod (z trzema przekładniami o wartościach od 0 do 9 - np. zamek do walizki/roweru). Będziemy mogli tworzyć obiekt typu Lock przekazując jako parametr konstruktora trzy cyfry - kod otwierający zamek.
Domyślnie obiekt będzie tworzony z prawidłowym ustawieniem przekładni. 
2. Obiekt typu Lock będzie oferował trzy metody zmieniający ustawienie każdej z przekładni o jedną pozycję w prawo, z uwzględnieniem tego, że gdy zmieniamy przekładnię o ustawieniu 9 w prawo, ustawimy ją w pozycji 0. Będzie również oferował metodę sprawdzającą, czy zamek jest otwarty, czy nie. Metoda toString zwróci aktualną kombinację zamka. 
3. Dodaj metodę shuffle - "mieszającą" aktualnie ustawiony kod.
4. Utwórz interfejs użytkownika. Wpierw spytaj użytkownika o prawidłowy kod. Następnie utwórz obiekt typu lock, po czym wywołaj jego metodę shuffle. Póki zamek nie będzie otwarty wypisuj na ekranie aktualną kombinację, po czym pytaj użytkownika, którą przekładnię chce przełożyć.
5. Zmodyfikuj klasę Lock oraz interfejs użytkownika tak, aby liczba przekładni mogła być dowolna.
