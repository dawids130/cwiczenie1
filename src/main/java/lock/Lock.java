package lock;

import java.util.Random;

public class Lock {
   // public Lock(int parseInt, int parseInt1, int parseInt2) {
    private int correctA;
    private int correctB;
    private int correctC;

    private int currentA;
    private int currentB;
    private int currentC;


    public Lock(int correctA, int correctB, int correctC){
    this.currentA = this.correctA = correctA;
    this.currentB = this.correctB = correctB;
    this.currentC = this.correctC = correctC;
    }


    public void switchA(){
       // if (currentA == 9){
       //     currentA = 0;
       // } else {
      //     currentA++;
      //  }
        currentA = (currentA + 1) % 10;
    }

    public void switchB(){
        if (currentB == 9){
            currentB = 0;
        } else {
            currentB++;
        }
    }

    public void switchC(){
        if (currentC == 9){
            currentC = 0;
        } else {
            currentC++;
        }
    }

    boolean isOpen(){
        System.out.println("Sprawdzam czy zamek jest otwarty");
        if (currentA == correctA && currentB == correctB && currentC == correctC ) {
            return true;
        } else {
            return false;
        }
    }

    public void shuffle(){
        Random random = new Random();
        currentA = random.nextInt(10);
        currentB = random.nextInt(10);
        currentC = random.nextInt(10);
        System.out.println("Po zamianie kombinacji ->> " + currentA + " " + currentB + " " + currentC);
    }

    @Override
    public String toString() {
        return "Aktualna kombinacja zamka: " + currentA + "-" + currentC + "-" + currentC;
    }
}
